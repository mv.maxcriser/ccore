package com.thecriser.ccore_example.sharedpreferences

import com.thecriser.ccore.sharedpreferences.SPreferences

class SPreferencesExample {

    fun example() {
        SPreferences.putDouble("TEST", 2.0)
        SPreferences.getDouble("TEST", 2.0)
    }
}