package com.thecriser.ccore_example.systemnotification

import com.thecriser.ccore.systemnotification.SystemNotificationData
import com.thecriser.ccore.systemnotification.SystemNotificationManager
import com.thecriser.ccore_example.R
import org.koin.core.KoinComponent
import org.koin.core.inject

class SystemNotificationExample : KoinComponent {

    private val manager: SystemNotificationManager by inject()

    fun example() {
        manager.show(
            SystemNotificationData(
                id = 1001,
                title = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                smallIcon = R.drawable.ic_launcher_foreground,
                autoCancel = false,
                notificationChannelName = "Application",
                action = null
            )
        )
    }
}