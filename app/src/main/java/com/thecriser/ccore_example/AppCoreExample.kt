package com.thecriser.ccore_example

import com.thecriser.ccore.AppCcore
import com.thecriser.ccore.deviceinfo.DeviceInfo
import com.thecriser.ccore.deviceinfo.IDeviceInfo
import com.thecriser.ccore.networking.NetworkService
import com.thecriser.ccore_example.koin.AppInfo
import com.thecriser.ccore_example.networking.NetworkApi
import com.thecriser.ccore_example.networking.headersFunction
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

class AppCoreExample : AppCcore() {

    override val koinModules = listOf(
        module {
            single { NetworkService(NetworkApi::class.java, headersFunction(get())) }

            single { AppInfo(androidContext()) }
            single<IDeviceInfo> { DeviceInfo(androidContext()) }
        }
    )

    override fun initAfterOnCreate() {
    }

    override fun initBeforeOnCreate() {
    }
}