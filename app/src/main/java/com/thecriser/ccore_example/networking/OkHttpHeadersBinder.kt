package com.thecriser.ccore_example.networking

import com.thecriser.ccore.deviceinfo.IDeviceInfo
import com.thecriser.ccore.networking.OkHttpHeader

class OkHttpHeadersBinder(private val deviceInfo: IDeviceInfo) : () -> List<OkHttpHeader> {

    override operator fun invoke(): List<OkHttpHeader> {
        val headers = mutableListOf<OkHttpHeader>()

        headers.add(OkHttpHeader("Device", deviceInfo.deviceId))
        headers.add(OkHttpHeader("Accept", "application/json"))
        headers.add(OkHttpHeader("Content-Type", "application/json"))

        return headers
    }
}

fun headersFunction(deviceInfo: IDeviceInfo): () -> List<OkHttpHeader> =
    OkHttpHeadersBinder(deviceInfo)