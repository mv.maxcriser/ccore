package com.thecriser.ccore_example.networking

import com.thecriser.ccore.networking.AbstractNetworkExecutable
import com.thecriser.ccore.networking.NetworkCallback

abstract class NetworkExecutable<MODEL>(
    callback: NetworkCallback<MODEL>
) : AbstractNetworkExecutable<MODEL, NetworkApi>(callback) {

    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }

    override val baseUrl = BASE_URL

}