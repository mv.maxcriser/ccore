package com.thecriser.ccore_example.networking

import retrofit2.Call
import retrofit2.http.GET

interface NetworkApi {

    @GET("todos")
    fun getTodos(): Call<List<TodoModel>>?

}