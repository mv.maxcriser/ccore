package com.thecriser.ccore_example.networking

import com.thecriser.ccore.networking.NetworkCallback
import org.koin.core.KoinComponent

class TodosNetworkExecutable(
    callback: NetworkCallback<List<TodoModel>>
) : NetworkExecutable<List<TodoModel>>(callback) {

    override fun call() = api().getTodos()

}