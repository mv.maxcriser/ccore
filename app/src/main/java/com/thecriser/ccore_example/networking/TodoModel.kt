package com.thecriser.ccore_example.networking

import com.google.gson.annotations.SerializedName

data class TodoModel(
    @SerializedName("userId") val userId: Long,
    @SerializedName("id") val id: Long,
    @SerializedName("title") val title: String? = null,
    @SerializedName("completed") val completed: Boolean = false
)