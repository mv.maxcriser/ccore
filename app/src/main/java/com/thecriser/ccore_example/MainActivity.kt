package com.thecriser.ccore_example

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.thecriser.ccore.CcoreBaseActivity
import com.thecriser.ccore.extension.toast
import com.thecriser.ccore.networking.NetworkCallback
import com.thecriser.ccore.networking.NetworkThrowable
import com.thecriser.ccore.permissions.CcorePermission
import com.thecriser.ccore.permissions.MultiplePermissionsReport
import com.thecriser.ccore.permissions.PermissionToken
import com.thecriser.ccore.permissions.listener.PermissionRequest
import com.thecriser.ccore.permissions.listener.multi.MultiplePermissionsListener
import com.thecriser.ccore_example.deviceinfo.DeviceInfoActivity
import com.thecriser.ccore_example.koin.AppInfo
import com.thecriser.ccore_example.networking.TodoModel
import com.thecriser.ccore_example.networking.TodosNetworkExecutable
import com.thecriser.ccore_example.systemnotification.SystemNotificationExample
import com.thecriser.ccore_example.ticker.TickerActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.core.inject


class MainActivity : CcoreBaseActivity(), NetworkCallback<List<TodoModel>> {

    private val appInfo: AppInfo by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showSystemNotification(view: View) {
        SystemNotificationExample().example()
    }

    fun executeFromNetwork(view: View) {
        TodosNetworkExecutable(this).execute()
    }

    override fun onSuccessful(response: List<TodoModel>?) {
        this.toast("onSuccessful=${response.toString()}")
    }

    override fun onFailure(throwable: NetworkThrowable) {
        this.toast("onFailure=${throwable.toString()}")
    }

    fun showDeviceInfo(view: View) {
        startActivity(Intent(this, DeviceInfoActivity::class.java))
    }

    fun showTickerExample(view: View) {
        startActivity(Intent(this, TickerActivity::class.java))
    }

    fun showTestKoin(view: View) {
        this.toast(appInfo.appName)
    }

    fun checkPermissions(view: View) {
        CcorePermission
            .withContext(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (!report.areAllPermissionsGranted()) {
                        toast("Для функционирования приложения вам необходимо предоставить доступ ко всем разрешениям.")
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {

                }
            })
            .check()
    }
}