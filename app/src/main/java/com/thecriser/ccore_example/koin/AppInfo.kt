package com.thecriser.ccore_example.koin

import android.content.Context
import com.thecriser.ccore_example.R

class AppInfo(context: Context) {

    val appName = context.getString(R.string.app_name)

}