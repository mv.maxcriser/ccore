package com.thecriser.ccore_example.sms

import android.content.Context
import com.thecriser.ccore.extension.toast
import com.thecriser.ccore.sms.CcoreSmsListener

class SmsListener : CcoreSmsListener() {

    override fun onMessageReceived(context: Context, messageBody: String) {
        context.toast(messageBody)
    }
}