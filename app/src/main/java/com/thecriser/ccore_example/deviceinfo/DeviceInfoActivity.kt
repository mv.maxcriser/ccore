package com.thecriser.ccore_example.deviceinfo

import android.os.Bundle
import com.thecriser.ccore.CcoreBaseActivity
import com.thecriser.ccore.deviceinfo.IDeviceInfo
import com.thecriser.ccore_example.R
import kotlinx.android.synthetic.main.activity_device_info.*
import org.koin.core.inject

class DeviceInfoActivity : CcoreBaseActivity() {

    private val deviceInfo: IDeviceInfo by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_info)

        init()
    }

    private fun init() {
        deviceInfoView.text = "Device ID: ${deviceInfo.deviceId}"
    }
}