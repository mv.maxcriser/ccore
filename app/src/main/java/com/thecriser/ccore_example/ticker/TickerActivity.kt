package com.thecriser.ccore_example.ticker

import android.os.Bundle
import com.thecriser.ccore.CcoreBaseActivity
import com.thecriser.ccore_example.R
import com.thecriser.ccore_example.koin.AppInfo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_ticker.*
import org.koin.core.inject


class TickerActivity : CcoreBaseActivity() {

    private val appInfo: AppInfo by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticker)


        initTickerView()
    }

    private fun initTickerView() {
        tickerView.setTextSize(16f)
        tickerView.setDisplacement(3)
        tickerView.pushTextData("<b>CCORE</b>")
        tickerView.showTickers()

        tickerViewOne.setTextSize(16f)
        tickerViewOne.setDisplacement(3)
        tickerViewOne.pushTextData("Example with one text data [1]")
        tickerViewOne.showTickers()

        tickerViewTwo.setTextSize(16f)
        tickerViewTwo.setDisplacement(3)
        tickerViewTwo.pushTextData("Example with two text data [1]")
        tickerViewTwo.pushTextData("Example with two text data [2]")
        tickerViewTwo.showTickers()

        tickerViewThree.setTextSize(16f)
        tickerViewThree.setDisplacement(3)
        tickerViewThree.pushTextData("<b>ccore</b>")
        tickerViewThree.pushTextData("<b>created</b>")
        tickerViewThree.pushTextData("<b>by</b>")
        tickerViewThree.pushTextData("<b>thecriser</b>")
        tickerViewThree.showTickers()

        tickerViewCont.setTextSize(16f)
        tickerViewCont.setDisplacement(3)
        tickerViewCont.pushTextData("continuous continuous continuous continuous continuous continuous continuous continuous")
        tickerViewCont.setContinuous(true)
        tickerViewCont.showTickers()
    }
}