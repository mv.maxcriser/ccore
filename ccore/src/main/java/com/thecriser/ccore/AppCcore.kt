package com.thecriser.ccore

import android.content.ContextWrapper
import androidx.multidex.MultiDexApplication
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.thecriser.ccore.koin.initKoin
import com.thecriser.ccore.sharedpreferences.SPreferences
import org.koin.core.module.Module

abstract class AppCcore : MultiDexApplication(), IAppCcore {

    override fun onCreate() {
        initBeforeOnCreate()

        super.onCreate()

        initAfterOnCreate()
        initSharedPreferences()

        initKoin(koinModules)
        initAppCenter(appCenterSecret)
    }

    private fun initAppCenter(secret: String?) {
        secret?.let {
            AppCenter.start(this, it, Analytics::class.java, Crashes::class.java)
        }
    }

    private fun initSharedPreferences() {
        SPreferences.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }

    abstract override val koinModules: List<Module>

    open val appCenterSecret: String? = null

    abstract override fun initAfterOnCreate()

    abstract override fun initBeforeOnCreate()

}
