package com.thecriser.ccore.deviceinfo

import android.content.Context

class DeviceInfo(context: Context) : IDeviceInfo {

    override val deviceId: String = android.provider.Settings.Secure.getString(context.contentResolver, android.provider.Settings.Secure.ANDROID_ID)

}