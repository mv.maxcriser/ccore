package com.thecriser.ccore.systemnotification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import org.koin.core.KoinComponent

class SystemNotificationManager(private val context: Context) : KoinComponent {

    private var manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

    companion object {
        const val CHANNEL_ID = "APPLICATION_CHANNEL_ID"
    }

    fun show(data: SystemNotificationData) {
        val id = data.id
        val stringID = data.id.toString()

        val notificationBuilder = NotificationCompat.Builder(context, stringID)

        data.let {
            notificationBuilder.apply {
                if (!it.title.isNullOrEmpty()) {
                    setContentTitle(it.title)
                }

                if (!it.message.isNullOrEmpty()) {
                    setContentText(it.message)
                    setStyle(NotificationCompat.BigTextStyle().bigText(it.message))
                }

                it.smallIcon?.apply {
                    setSmallIcon(this)
                }

                it.largeIcon?.apply {
                    setLargeIcon(this)
                }

                it.whenTime?.apply {
                    setWhen(this)
                }

                it.action?.apply {
                    addAction(this.icon, this.text, this.pendingIntent)
                    setContentIntent(this.pendingIntent)
                }

                setChannelId(CHANNEL_ID)
                setAutoCancel(it.autoCancel)
                setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_HIGH
                val channel = NotificationChannel(CHANNEL_ID, it.notificationChannelName, importance)

                manager?.createNotificationChannel(channel)
            }

            manager?.notify(id, notificationBuilder.build())
        }
    }
}
