package com.thecriser.ccore.systemnotification

import android.graphics.Bitmap

data class SystemNotificationData(
    val id: Int = 0,
    val title: String? = null,
    val message: String? = null,
    val whenTime: Long? = null,
    val smallIcon: Int? = null,
    val largeIcon: Bitmap? = null,
    val autoCancel: Boolean = true,
    val notificationChannelName: String = "",
    val action: NotificationAction? = null
)
