package com.thecriser.ccore.systemnotification

import android.app.PendingIntent

data class NotificationAction(
    val icon: Int = 0,
    val text: String,
    val pendingIntent: PendingIntent
)