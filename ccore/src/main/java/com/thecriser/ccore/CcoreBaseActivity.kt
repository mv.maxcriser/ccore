package com.thecriser.ccore

import androidx.appcompat.app.AppCompatActivity
import org.koin.core.KoinComponent

open class CcoreBaseActivity : AppCompatActivity(), KoinComponent {

}