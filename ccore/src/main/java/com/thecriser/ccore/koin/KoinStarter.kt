package com.thecriser.ccore.koin

import android.app.Application
import androidx.multidex.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module

fun Application.initKoin(modules: List<Module>) {

    startKoin {
        androidContext(this@initKoin)
        androidLogger()

        modules.toMutableList()

        modules(modules)
    }
}
