package com.thecriser.ccore.sound

import android.content.Context
import android.media.MediaPlayer
import android.os.VibrationEffect
import android.os.Vibrator
import com.thecriser.ccore.util.VersionUtil


class SoundManager(val context: Context) {

    var mp: MediaPlayer? = null

    fun play(soundEffect: SoundEffect) {
        mp = MediaPlayer.create(context, soundEffect.resId);
        mp?.start()
    }

    fun vibrate(effect: VibroEffect) {
        val v: Vibrator? = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?

        if (VersionUtil.hasOreo()) {
            v?.vibrate(VibrationEffect.createOneShot(effect.duration, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v?.vibrate(effect.duration)
        }

    }
}
