package com.thecriser.ccore.sound

import androidx.annotation.RawRes
import com.thecriser.ccore.R

enum class SoundEffect(@RawRes val resId: Int) {

    WIN(R.raw.win),
    LOSE(R.raw.lose),
    OVER_TIMER(R.raw.over_timer),
    MOUSE_CLICK(R.raw.mouse_click)

}
