package com.thecriser.ccore.sound

enum class VibroEffect(val duration: Long) {

    SHORT(200),
    MEDIUM(400),
    LONG(800)

}
