package com.thecriser.ccore.ui.action


class ActionProcessorAdapter<I : Action, O : Action>(
    private val consumer: ActionProcessor<O>,
    val actionMapper: (I) -> O
) : ActionProcessor<I> {
    override fun processAction(action: I) = consumer.processAction(actionMapper(action))
}

@Suppress("NOTHING_TO_INLINE")
inline fun <I : Action, O : Action> ActionProcessor<O>.adapt(
    noinline actionMapper: (I) -> O
): ActionProcessor<I> = ActionProcessorAdapter(
    this,
    actionMapper
)
