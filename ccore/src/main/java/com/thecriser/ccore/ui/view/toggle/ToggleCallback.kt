package com.thecriser.ccore.ui.view.toggle

interface ToggleCallback<T> {

    fun onChange(selection: T)

}