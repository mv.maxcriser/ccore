package com.thecriser.ccore.ui.diff

import androidx.recyclerview.widget.DiffUtil


open class SimpleDiffUtilItemCallback<T : Differentiable> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem.differentiableId == newItem.differentiableId

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem.isSameAs(newItem)

    override fun getChangePayload(oldItem: T, newItem: T): Any? = oldItem.getChangePayload(newItem)
}
