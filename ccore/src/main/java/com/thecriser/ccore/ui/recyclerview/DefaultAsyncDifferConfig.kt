package com.thecriser.ccore.ui.recyclerview

import androidx.recyclerview.widget.AsyncDifferConfig
import com.thecriser.ccore.ui.diff.DiffUtilItemCallbackFactory
import com.thecriser.ccore.ui.diff.Differentiable
import com.thecriser.ccore.ui.diff.SimpleDiffUtilItemCallbackFactory

@Suppress("FunctionName")
fun <T : Differentiable> DefaultAsyncDifferConfig(
    diffUtilItemCallbackFactory: DiffUtilItemCallbackFactory<T> = SimpleDiffUtilItemCallbackFactory()
) = AsyncDifferConfig
    .Builder(diffUtilItemCallbackFactory.getDiffUtilItemCallback())
    .build()
