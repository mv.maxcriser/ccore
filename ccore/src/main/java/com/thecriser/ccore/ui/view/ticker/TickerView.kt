package com.thecriser.ccore.ui.view.ticker

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.HorizontalScrollView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.thecriser.ccore.extension.parseHtml
import java.util.*

class TickerView : HorizontalScrollView {

    companion object {
        const val TAG = "TickerView"
    }

    private var displacement = 1L
    private var textSize = 16f
    private var scrollPos = 0
    private var scrollTimer: Timer? = null
    private var scrollerSchedule: TimerTask? = null
    private var lastTicker: TextView? = null
    private var childViews: MutableList<View>? = null
    private var linearLayout: LinearLayout? = null
    private var screenWidth: Int = 0
    private var continuous: Boolean = false

    constructor(
        context: Context
    ) : super(context) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        showTickers()
    }

    override fun onDetachedFromWindow() {
        destroyAllScheduledTasks()

        super.onDetachedFromWindow()
    }

    private fun init(context: Context) {
        screenWidth = (context as Activity).windowManager.defaultDisplay.width

        val linearLayout = LinearLayout(context)
        linearLayout.layoutParams = LinearLayout.LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )

        linearLayout.orientation = LinearLayout.HORIZONTAL
        linearLayout.gravity = Gravity.CENTER_VERTICAL

        isHorizontalFadingEdgeEnabled = false
        isVerticalFadingEdgeEnabled = false
        foregroundGravity = Gravity.CENTER_VERTICAL
        isHorizontalScrollBarEnabled = false

        setDisplacement(displacement)
        setTextSize(textSize)
    }

    fun getDisplacement(): Long {
        return displacement
    }

    fun getTextSize(): Float {
        return textSize
    }

    fun setContinuous(continuous: Boolean) {
        this.continuous = continuous
    }

    fun setDisplacement(displacement: Long) {
        Log.d(TAG, "setDisplacement")

        this.displacement = displacement
    }

    fun setTextSize(textSize: Float) {
        Log.d(TAG, "setTextSize")

        this.textSize = textSize
    }

    fun setChildViews(childViews: MutableList<View>?) {
        this.childViews = childViews
    }

    fun clear() {
        Log.d(TAG, "clear")

        childViews?.clear()
    }

    private fun addChildView(childView: View) {
        Log.d(TAG, "addChildView")

        if (childViews == null) {
            childViews = ArrayList()
        }

        childViews!!.add(childView)
    }

    fun pushTextData(text: String, textColor: Int = Color.BLACK) {
        Log.d(TAG, "pushTextData")

        val tv = TextView(context)

        tv.layoutParams = LinearLayout.LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        tv.text = text.parseHtml()
        tv.setTextColor(textColor)
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)

        addChildView(tv)
    }

    fun showTickers() {
        Log.d(TAG, "showTickers")

        linearLayout?.removeAllViews()

        removeAllViewsInLayout()
        linearLayout = LinearLayout(context)

        val lpPar = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        val lpPar0 = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        val lpParLast = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        linearLayout?.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        lpPar.setMargins(10, 3, 5, 3)
        lpPar0.setMargins(screenWidth, 3, 5, 3)
        lpParLast.setMargins(5, 3, screenWidth, 3)

        if (childViews != null && childViews!!.isNotEmpty()) {
            for (index in childViews!!.indices) {
                when (index) {
                    0 -> linearLayout!!.addView(childViews!![index], lpPar0)
                    childViews!!.size - 1 -> linearLayout!!.addView(childViews!![index], lpParLast)
                    else -> linearLayout!!.addView(childViews!![index], lpPar)
                }
            }

            lastTicker = TextView(context)
            lastTicker?.layoutParams = LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
            )

            lastTicker?.visibility = View.INVISIBLE
            linearLayout?.addView(lastTicker, lpPar)

            val vto = viewTreeObserver

            vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                    startAutoScrolling(width)
                }
            })

            addView(linearLayout)
        }
    }

    private fun setPaddings(width: Int) {
        Log.d(TAG, "setPaddings")

        childViews?.apply {
            val size = this.size

            if (size == 1) {
                this[0].setPadding(0, 0, width, 0)
            } else {
                for (i in 0..size) {
                    if (i < size - 1) {
                        this[i].setPadding(0, 0, width, 0)
                    }
                }
            }
        }
    }

    fun startAutoScrolling(width: Int) {
        Log.d(TAG, "startAutoScrolling")

        if (!continuous) {
            setPaddings(width)
        }

        if (scrollTimer != null) {
            scrollTimer!!.cancel()
        }

        scrollTimer = Timer()

        val timerTick = Runnable { moveScrollView() }

        if (scrollerSchedule != null) {
            scrollerSchedule!!.cancel()
            scrollerSchedule = null
        }

        scrollerSchedule = object : TimerTask() {
            override fun run() {
                try {
                    (context as Activity).runOnUiThread(timerTick)
                } catch (e: Exception) {
                    e.printStackTrace()
                    this.cancel()
                }
            }
        }

        if (displacement > 0) {
            scrollTimer?.schedule(scrollerSchedule, 0, displacement)
        }
    }

    @Synchronized
    private fun moveScrollView() {
//        Log.d(TAG, "moveScrollView")

        try {
            scrollPos = scrollX + 1

//            Log.d(TAG, "scrollPos=$scrollPos \t scrollX=$scrollX \t displacement=$displacement")

            val bounds = Rect()

            lastTicker?.getHitRect(bounds)

            val scrollBounds = Rect(scrollX, scrollY, scrollX + width, scrollY + height)

            if (Rect.intersects(scrollBounds, bounds)) {
                scrollPos = 0
                scrollTo(scrollPos, 0)
            } else {
                scrollTo(scrollPos, 0)
            }

//            Log.d(TAG, "scrollPos=$scrollPos")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun destroyAllScheduledTasks() {
        Log.d(TAG, "destroyAllScheduledTasks")

        clearTimerTasks(scrollerSchedule)
        clearTimers(scrollTimer)

        scrollerSchedule = null
        scrollTimer = null
    }

    private fun clearTimers(timer: Timer?) {
        Log.d(TAG, "clearTimers")

        timer?.cancel()
    }

    private fun clearTimerTasks(timerTask: TimerTask?) {
        Log.d(TAG, "clearTimerTasks")

        timerTask?.cancel()
    }
}