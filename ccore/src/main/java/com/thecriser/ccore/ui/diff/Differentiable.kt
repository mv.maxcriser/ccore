package com.thecriser.ccore.ui.diff


interface Differentiable {


    val differentiableId: Any


    fun isSameAs(other: Differentiable) = this == other

    fun getChangePayload(other: Differentiable): Any? = null

    object NoId
}
