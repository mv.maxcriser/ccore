package com.thecriser.ccore.ui.view.toggle

import com.thecriser.ccore.ui.action.Action

sealed class ToggleAction : Action {
    data class OnItemSelected<T : ToggleItemUiModel>(val item: T) : ToggleAction()
}
