package com.thecriser.ccore.ui.view.toggle

import android.view.ViewGroup
import com.thecriser.ccore.ui.action.ActionProcessor
import com.thecriser.ccore.ui.recyclerview.BaseAdapter

class ToggleAdapter<M : ToggleItemUiModel, VH : ToggleViewHolder<M>>(
    private val callback: ToggleCallback<M>?,
    private val viewHolderFactory: (parent: ViewGroup, callback: ToggleCallback<M>?) -> VH
) : BaseAdapter<ToggleItem<M>, VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH = viewHolderFactory(parent, callback)

    override fun submitList(list: MutableList<ToggleItem<M>>?) {
        super.submitList(list?.mapTo(ArrayList()) { it })
    }
}

