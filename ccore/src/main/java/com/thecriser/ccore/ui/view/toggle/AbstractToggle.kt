package com.thecriser.ccore.ui.view.toggle

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.withStyledAttributes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thecriser.ccore.R
import com.thecriser.ccore.extension.withValueIfExist
import com.thecriser.ccore.ui.action.ActionProcessor
import kotlinx.android.synthetic.main.view_toggle.view.*

abstract class AbstractToggle<T : ToggleItemUiModel, M : ToggleUiModel<T>, VH : ToggleViewHolder<T>> @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), ActionProcessor<ToggleAction> {

    abstract val holderCreator: (
        parent: ViewGroup,
        callback: ToggleCallback<T>?
    ) -> VH

    var callback: ToggleCallback<T>? = null

    private val innerCallback = object : ToggleCallback<T> {
        override fun onChange(selection: T) {
            resolveItemSelection(selection)

            callback?.onChange(selection)
        }
    }

    protected var uiModel: M? = null

    protected var toggleAdapter: ToggleAdapter<T, VH>? = null

    protected val items = ArrayList<ToggleItem<T>>()

    protected var spanCount: Int = DEFAULT_SPAN_COUNT

    init {
        LayoutInflater.from(context).inflate(R.layout.view_toggle, this, true)

        context.withStyledAttributes(attrs, R.styleable.AbstractToggle) {
            withValueIfExist(
                R.styleable.AbstractToggle_spanCount, TypedArray::getInt,
                DEFAULT_SPAN_COUNT
            ) {
                spanCount = it
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        uiModel?.apply {
            setModel(this)
        }
    }

    override fun processAction(action: ToggleAction) =
        when (action) {
            is ToggleAction.OnItemSelected<*> -> resolveItemSelection(action.item)
        }

    fun setModel(uiModel: M, selection: T? = null) {
        spanCount = uiModel.items.size

        initRecycler()

        val selectedItem = selection ?: items.find { it.isSelected && !it.isDisabled }?.uiModel
            ?: uiModel.items.firstOrNull { !it.disabled }

        items.clear()
        items.addAll(uiModel.items.map {
            ToggleItem(
                it,
                it.differentiableId == selectedItem?.differentiableId
            )
        })
        this.uiModel = uiModel
        renderUiModel(uiModel)
    }

    protected fun renderUiModel(uiModel: M) {
        toggleAdapter?.submitList(items)
    }

    private fun resolveItemSelection(item: ToggleItemUiModel) {
        if (items.find { it.isSelected }?.differentiableId == item.differentiableId || item.disabled) return
        val newItems = items.map {
            if (it.differentiableId == item.differentiableId) it.copy(isSelected = true)
            else it.copy(isSelected = false)
        }
        items.clear()
        items.addAll(newItems)

        toggleAdapter?.submitList(items)
    }

    private fun initRecycler() {
        toggleAdapter = ToggleAdapter(innerCallback, holderCreator)

        toggleItemsRecyclerView.layoutManager = layoutManager

            buildItemDecoration(spanCount)?.apply {
            toggleItemsRecyclerView.addItemDecoration(this)
        }

        toggleItemsRecyclerView.adapter = toggleAdapter
    }

    abstract fun buildItemDecoration(itemsCount: Int): RecyclerView.ItemDecoration?

    abstract val layoutManager: RecyclerView.LayoutManager

    companion object {
        protected const val DEFAULT_SPAN_COUNT = 2
    }
}
