package com.thecriser.ccore.ui.view.toggle

import com.thecriser.ccore.ui.diff.Differentiable

interface ToggleItemUiModel : Differentiable {
    val textRes: Int
    val disabled: Boolean
}
