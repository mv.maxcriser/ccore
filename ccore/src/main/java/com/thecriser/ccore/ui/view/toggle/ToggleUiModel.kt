package com.thecriser.ccore.ui.view.toggle

open class ToggleUiModel<T : ToggleItemUiModel>(val items: List<T>)
