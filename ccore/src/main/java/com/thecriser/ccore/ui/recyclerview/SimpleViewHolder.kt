package com.thecriser.ccore.ui.recyclerview

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.thecriser.ccore.ui.action.Action
import com.thecriser.ccore.ui.action.ActionProcessor
import com.thecriser.ccore.ui.diff.Differentiable

abstract class SimpleViewHolder<T : Differentiable, A : Action, P : ActionProcessor<A>>(
    parent: ViewGroup,
    @LayoutRes layoutResId: Int,
    protected val actionProcessor: P
) : BaseViewHolder<T>(parent, layoutResId)

