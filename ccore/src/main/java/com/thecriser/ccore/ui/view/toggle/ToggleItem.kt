package com.thecriser.ccore.ui.view.toggle

import com.thecriser.ccore.ui.diff.Differentiable

data class ToggleItem<M : ToggleItemUiModel>(
    val uiModel: M,
    var isSelected: Boolean = false
) : Differentiable {
    override val differentiableId = uiModel.differentiableId

    val isDisabled = uiModel.disabled
}
