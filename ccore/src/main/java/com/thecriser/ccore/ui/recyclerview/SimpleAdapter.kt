package com.thecriser.ccore.ui.recyclerview

import android.view.ViewGroup
import com.thecriser.ccore.ui.action.Action
import com.thecriser.ccore.ui.action.ActionProcessor
import com.thecriser.ccore.ui.diff.Differentiable

open class SimpleAdapter<T : Differentiable, A: Action, P : ActionProcessor<A>, F : ViewHolderFactory<T, A, P, out SimpleViewHolder<T, A, P>>>(
    private val holderFactory: F,
    private val actionProcessor: P
) :
    BaseAdapter<T, SimpleViewHolder<T, A, P>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder<T, A, P> =
        holderFactory.create(parent, actionProcessor, viewType)
}
