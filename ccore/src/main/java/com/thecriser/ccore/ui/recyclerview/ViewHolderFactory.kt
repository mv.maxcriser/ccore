package com.thecriser.ccore.ui.recyclerview

import android.view.ViewGroup
import com.thecriser.ccore.ui.action.Action
import com.thecriser.ccore.ui.action.ActionProcessor
import com.thecriser.ccore.ui.diff.Differentiable

abstract class ViewHolderFactory<T : Differentiable, A : Action, P : ActionProcessor<A>, VH: SimpleViewHolder<T, A, P>> {
    abstract fun create(parent: ViewGroup, processor: P, viewType: Int): SimpleViewHolder<T, A, P>
}
