package com.thecriser.ccore.ui.action


interface ActionProcessor<A: Action> {
    fun processAction(action: A)
}
