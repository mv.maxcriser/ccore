package com.thecriser.ccore.ui.view.toggle

import android.content.Context
import android.view.ViewGroup
import com.thecriser.ccore.ui.recyclerview.BaseViewHolder

abstract class ToggleViewHolder<M : ToggleItemUiModel>(
    parent: ViewGroup,
    layout: Int,
    private val callback: ToggleCallback<M>?
) : BaseViewHolder<ToggleItem<M>>(parent, layout) {

    override fun bind(item: ToggleItem<M>, payloads: MutableList<Any>?) =
        with(itemView) {
            setOnClickListener {
                if (!item.isSelected) {
                    callback?.onChange(item.uiModel)
                }
            }

            when {
                item.isDisabled -> renderDisable(context, item.uiModel)
                item.isSelected -> renderSelected(context, item.uiModel)
                else -> renderDefault(context, item.uiModel)
            }
        }

    abstract fun renderSelected(context: Context, item: M)

    abstract fun renderDefault(context: Context, item: M)

    abstract fun renderDisable(context: Context, item: M)
}
