package com.thecriser.ccore.ui.recyclerview

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.annotation.Px
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class SpaceItemDecoration(
    @Px private val leftSpace: Int,
    @Px private val topSpace: Int,
    @Px private val rightSpace: Int,
    @Px private val bottomSpace: Int,
    private val showFirstVerticalDivider: Boolean = false,
    private val showLastVerticalDivider: Boolean = false,
    private val showFirstHorizontalDivider: Boolean = false,
    private val showLastHorizontalDivider: Boolean = false
) : RecyclerView.ItemDecoration() {

    constructor(
        @Px space: Int,
        showFirstVerticalDivider: Boolean = false,
        showLastVerticalDivider: Boolean = false,
        showFirstHorizontalDivider: Boolean = false,
        showLastHorizontalDivider: Boolean = false
    ) : this(
        leftSpace = space,
        topSpace = space,
        rightSpace = space,
        bottomSpace = space,
        showFirstVerticalDivider = showFirstVerticalDivider,
        showLastVerticalDivider = showLastVerticalDivider,
        showFirstHorizontalDivider = showFirstHorizontalDivider,
        showLastHorizontalDivider = showLastHorizontalDivider
    )

    
    constructor(configuration: Configuration) : this(
        leftSpace = configuration.leftSpace,
        topSpace = configuration.topSpace,
        rightSpace = configuration.rightSpace,
        bottomSpace = configuration.bottomSpace,
        showFirstVerticalDivider = configuration.showFirstVerticalDivider,
        showLastVerticalDivider = configuration.showLastVerticalDivider,
        showFirstHorizontalDivider = configuration.showFirstHorizontalDivider,
        showLastHorizontalDivider = configuration.showLastHorizontalDivider
    )

    private var orientation = -1
    private var spanCount = 1

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (leftSpace != 0 || topSpace != 0 || rightSpace != 0 || bottomSpace != 0) {
            if (orientation == -1) orientation = getOrientation(parent)
            makeOffsets(outRect, view, parent, state)
        }
    }

    private fun makeOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State): Rect =
        when (val layoutManager = parent.layoutManager) {
            is GridLayoutManager -> {
                spanCount = layoutManager.spanCount
                makeGridOffsets(outRect, view, parent, state)
            }
            is LinearLayoutManager -> {
                makeLinearOffsets(outRect, view, parent, state)
            }
            else -> throw IllegalStateException("RecyclerView have unsupported layout manager")
        }

    
    private fun makeGridOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State): Rect =
        outRect.apply {
            val column = parent.getChildAdapterPosition(view) % spanCount
            val row = parent.getChildAdapterPosition(view) / spanCount

            if (column != RecyclerView.NO_POSITION && row != RecyclerView.NO_POSITION) {
                val lastRow = state.itemCount / spanCount
                val lastColumn = spanCount - 1
                var rightOffsetRequired = false
                var bottomOffsetRequired = false

                when (orientation) {
                    VERTICAL -> {
                        rightOffsetRequired = column == lastColumn
                        bottomOffsetRequired = row == lastRow
                    }
                    HORIZONTAL -> {
                        rightOffsetRequired = row == lastRow
                        bottomOffsetRequired = column == lastColumn
                    }
                }

                // Если элемент не в последней строке/столбце выставляем отступ справа
                if (!rightOffsetRequired || showLastVerticalDivider) {
                    right = rightSpace
                }
                // Если элемент не в последнем столбце/строке выставляем отступ снизу
                if (!bottomOffsetRequired || showLastHorizontalDivider) {
                    bottom = bottomSpace
                }
                // отступы сверху и слева выставляем по необходимости
                if (row == 0 && showFirstVerticalDivider) {
                    left = leftSpace
                }
                if (column == 0 && showFirstVerticalDivider) {
                    top = topSpace
                }
            }
        }

    
    private fun makeLinearOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State): Rect =
        outRect.apply {
            val position = parent.getChildAdapterPosition(view)
            if (position != RecyclerView.NO_POSITION) {
                when (orientation) {
                    VERTICAL -> {
                        if (position != 0 || showFirstHorizontalDivider) top = topSpace
                        if (position == state.itemCount - 1 && showLastHorizontalDivider) bottom = bottomSpace
                    }
                    HORIZONTAL -> {
                        if (position != 0 || showFirstVerticalDivider) left = leftSpace
                        if (position == state.itemCount - 1 && showLastVerticalDivider) right = rightSpace
                    }
                }
            }
        }

    private fun getOrientation(parent: RecyclerView): Int =
        if (parent.layoutManager is LinearLayoutManager) {
            (parent.layoutManager as LinearLayoutManager).orientation
        } else {
            throw IllegalStateException(
                "DividerItemDecoration can only be used with a LinearLayoutManager or its inheritor."
            )
        }

    companion object {
        private const val VERTICAL = RecyclerView.VERTICAL
        private const val HORIZONTAL = RecyclerView.HORIZONTAL
    }

    
    class Configuration(private val resources: Resources) {
        
        @Px
        var leftSpace: Int = 0

        
        @Px
        var topSpace: Int = 0

        
        @Px
        var rightSpace: Int = 0

        
        @Px
        var bottomSpace: Int = 0

        
        var showFirstVerticalDivider: Boolean = false

        
        var showLastVerticalDivider: Boolean = false

        
        var showFirstHorizontalDivider: Boolean = false

        
        var showLastHorizontalDivider: Boolean = false

        
        @Px
        fun fromDimen(@DimenRes dimenResId: Int): Int = resources.getDimensionPixelSize(dimenResId)

        
        fun setSpace(@Px space: Int) {
            leftSpace = space
            topSpace = space
            rightSpace = space
            bottomSpace = space
        }
    }
}
