package com.thecriser.ccore.ui.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    constructor(parent: ViewGroup, @LayoutRes layoutResId: Int) : this(
        LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
    )

    abstract fun bind(item: T, payloads: MutableList<Any>? = null)
}
