package com.thecriser.ccore.ui.recyclerview

import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import com.thecriser.ccore.ui.diff.Differentiable

abstract class BaseAdapter<T : Differentiable, VH : BaseViewHolder<T>>(
    asyncDifferConfig: AsyncDifferConfig<T> = DefaultAsyncDifferConfig()
) : ListAdapter<T, VH>(asyncDifferConfig) {

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(getItem(position))
    }
}

