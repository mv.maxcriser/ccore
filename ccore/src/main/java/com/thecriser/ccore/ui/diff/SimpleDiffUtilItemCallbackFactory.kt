package com.thecriser.ccore.ui.diff

import androidx.recyclerview.widget.DiffUtil

open class SimpleDiffUtilItemCallbackFactory<T : Differentiable> : DiffUtilItemCallbackFactory<T> {
    override fun getDiffUtilItemCallback(): DiffUtil.ItemCallback<T> = SimpleDiffUtilItemCallback()
}
