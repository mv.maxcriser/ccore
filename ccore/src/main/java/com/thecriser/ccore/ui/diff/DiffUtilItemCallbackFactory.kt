package com.thecriser.ccore.ui.diff

import androidx.recyclerview.widget.DiffUtil


interface DiffUtilItemCallbackFactory<T> {

    fun getDiffUtilItemCallback(): DiffUtil.ItemCallback<T>
}
