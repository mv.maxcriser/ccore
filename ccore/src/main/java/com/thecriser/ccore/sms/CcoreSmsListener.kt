package com.thecriser.ccore.sms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log

abstract class CcoreSmsListener : BroadcastReceiver(), ISmsListener {

    companion object {
        const val TAG = "CcoreSmsListener"
        const val INTENT_ACTION = "android.provider.Telephony.SMS_RECEIVED"
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == INTENT_ACTION) {
            val bundle = intent.extras
            val messages: Array<SmsMessage?>

            if (bundle != null) {
                try {
                    val pdus = bundle["pdus"] as Array<*>?

                    messages = arrayOfNulls(pdus!!.size)

                    for (i in messages.indices) {
                        messages[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                        val msgBody = messages[i]?.messageBody

                        if (!msgBody.isNullOrEmpty()) {
                            onMessageReceived(context, msgBody)
                        }
                    }
                } catch (e: Exception) {
                    Log.e(TAG, e.message ?: "EMPTY ERROR")
                }
            }
        }
    }
}