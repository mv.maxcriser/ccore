package com.thecriser.ccore.sms

import android.content.Context

interface ISmsListener {
    
    fun onMessageReceived(context: Context, messageBody: String)
    
}