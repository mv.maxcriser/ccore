package com.thecriser.ccore

import androidx.fragment.app.FragmentActivity
import org.koin.core.KoinComponent

open class CcoreBaseFragment : FragmentActivity(), KoinComponent {

}