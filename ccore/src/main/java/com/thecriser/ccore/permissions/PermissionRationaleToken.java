package com.thecriser.ccore.permissions;

final class PermissionRationaleToken implements PermissionToken {

  private final CcorePermissionInstance ccorePermissionInstance;
  private boolean isTokenResolved = false;

  PermissionRationaleToken(CcorePermissionInstance ccorePermissionInstance) {
    this.ccorePermissionInstance = ccorePermissionInstance;
  }

  @Override public void continuePermissionRequest() {
    if (!isTokenResolved) {
      ccorePermissionInstance.onContinuePermissionRequest();
      isTokenResolved = true;
    }
  }

  @Override public void cancelPermissionRequest() {
    if (!isTokenResolved) {
      ccorePermissionInstance.onCancelPermissionRequest();
      isTokenResolved = true;
    }
  }
}
