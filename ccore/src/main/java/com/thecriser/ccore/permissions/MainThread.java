package com.thecriser.ccore.permissions;

import android.os.Handler;
import android.os.Looper;

final class MainThread implements Thread {

    MainThread() {

    }

    @Override
    public void execute(Runnable runnable) {
        if (runningMainThread()) {
            runnable.run();
        } else {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    @Override
    public void loop() {
    }

    private static boolean runningMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
