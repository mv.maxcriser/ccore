package com.thecriser.ccore.permissions;

import com.thecriser.ccore.permissions.listener.CcorePermissionError;

final class CcorePermissionException extends IllegalStateException {

    final CcorePermissionError error;

    CcorePermissionException(String detailMessage, CcorePermissionError error) {
      super(detailMessage);
        this.error = error;
    }
}
