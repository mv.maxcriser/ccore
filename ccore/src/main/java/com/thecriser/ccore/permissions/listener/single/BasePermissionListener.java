package com.thecriser.ccore.permissions.listener.single;

import com.thecriser.ccore.permissions.PermissionToken;
import com.thecriser.ccore.permissions.listener.PermissionDeniedResponse;
import com.thecriser.ccore.permissions.listener.PermissionGrantedResponse;
import com.thecriser.ccore.permissions.listener.PermissionRequest;

public class BasePermissionListener implements PermissionListener {

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {

    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {

    }

    @Override
    public void onPermissionRationaleShouldBeShown(
            PermissionRequest permission,
            PermissionToken token
    ) {
        token.continuePermissionRequest();
    }
}
