package com.thecriser.ccore.permissions;

import com.thecriser.ccore.permissions.listener.PermissionDeniedResponse;
import com.thecriser.ccore.permissions.listener.PermissionGrantedResponse;

import java.util.LinkedList;
import java.util.List;

public final class MultiplePermissionsReport {

    private final List<PermissionGrantedResponse> grantedPermissionResponses;
    private final List<PermissionDeniedResponse> deniedPermissionResponses;

    MultiplePermissionsReport() {
        grantedPermissionResponses = new LinkedList<>();
        deniedPermissionResponses = new LinkedList<>();
    }

    public List<PermissionGrantedResponse> getGrantedPermissionResponses() {
        return grantedPermissionResponses;
    }

    public List<PermissionDeniedResponse> getDeniedPermissionResponses() {
        return deniedPermissionResponses;
    }

    public boolean areAllPermissionsGranted() {
        return deniedPermissionResponses.isEmpty();
    }

    public boolean isAnyPermissionPermanentlyDenied() {
        boolean hasPermanentlyDeniedAnyPermission = false;

        for (PermissionDeniedResponse deniedResponse : deniedPermissionResponses) {
            if (deniedResponse.isPermanentlyDenied()) {
                hasPermanentlyDeniedAnyPermission = true;
                break;
            }
        }

        return hasPermanentlyDeniedAnyPermission;
    }

    boolean addGrantedPermissionResponse(PermissionGrantedResponse response) {
        return grantedPermissionResponses.add(response);
    }

    boolean addDeniedPermissionResponse(PermissionDeniedResponse response) {
        return deniedPermissionResponses.add(response);
    }

    void clear() {
        grantedPermissionResponses.clear();
        deniedPermissionResponses.clear();
    }
}
