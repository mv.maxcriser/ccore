package com.thecriser.ccore.permissions.listener.multi;

import com.thecriser.ccore.permissions.MultiplePermissionsReport;
import com.thecriser.ccore.permissions.PermissionToken;
import com.thecriser.ccore.permissions.listener.PermissionRequest;

import java.util.List;

public interface MultiplePermissionsListener {

    void onPermissionsChecked(MultiplePermissionsReport report);

    void onPermissionRationaleShouldBeShown(
            List<PermissionRequest> permissions,
            PermissionToken token
    );
}
