package com.thecriser.ccore.permissions;

import android.os.Looper;

final class ThreadFactory {

    public static Thread makeMainThread() {
        return new MainThread();
    }

    public static Thread makeSameThread() {
        if (runningMainThread()) {
            return new MainThread();
        } else {
            return new WorkerThread();
        }
    }

    private static boolean runningMainThread() {
        return Looper.getMainLooper().getThread() == java.lang.Thread.currentThread();
    }
}
