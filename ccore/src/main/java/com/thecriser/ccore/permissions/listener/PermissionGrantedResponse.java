package com.thecriser.ccore.permissions.listener;

import androidx.annotation.NonNull;

public final class PermissionGrantedResponse {

    private final PermissionRequest requestedPermission;

    public PermissionGrantedResponse(@NonNull PermissionRequest requestedPermission) {
        this.requestedPermission = requestedPermission;
    }

    public static PermissionGrantedResponse from(@NonNull String permission) {
        return new PermissionGrantedResponse(new PermissionRequest(permission));
    }

    public PermissionRequest getRequestedPermission() {
        return requestedPermission;
    }

  public String getPermissionName() {
        return requestedPermission.getName();
    }
}
