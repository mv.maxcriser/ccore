package com.thecriser.ccore.permissions.listener;

public interface PermissionRequestErrorListener {
  void onError(CcorePermissionError error);
}
