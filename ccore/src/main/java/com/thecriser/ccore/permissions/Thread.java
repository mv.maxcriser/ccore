package com.thecriser.ccore.permissions;

interface Thread {
    void execute(Runnable runnable);

    void loop();
}
