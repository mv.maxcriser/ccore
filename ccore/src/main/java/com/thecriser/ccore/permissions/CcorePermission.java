package com.thecriser.ccore.permissions;

import android.app.Activity;
import android.content.Context;

import com.thecriser.ccore.permissions.listener.EmptyPermissionRequestErrorListener;
import com.thecriser.ccore.permissions.listener.PermissionRequestErrorListener;
import com.thecriser.ccore.permissions.listener.multi.BaseMultiplePermissionsListener;
import com.thecriser.ccore.permissions.listener.multi.MultiplePermissionsListener;
import com.thecriser.ccore.permissions.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public final class CcorePermission implements CcorePermissionBuilder, CcorePermissionBuilder.Permission, CcorePermissionBuilder.SinglePermissionListener,
        CcorePermissionBuilder.MultiPermissionListener {

    private static CcorePermissionInstance instance;

    private Collection<String> permissions;
    private MultiplePermissionsListener listener = new BaseMultiplePermissionsListener();
    private PermissionRequestErrorListener errorListener = new EmptyPermissionRequestErrorListener();
    private boolean shouldExecuteOnSameThread = false;

    private CcorePermission(Context context) {
        initialize(context);
    }

    @Deprecated
    public static CcorePermissionBuilder.Permission withActivity(Activity activity) {
        return new CcorePermission(activity);
    }

    public static CcorePermissionBuilder.Permission withContext(Context context) {
        return new CcorePermission(context);
    }

    @Override
    public CcorePermissionBuilder.SinglePermissionListener withPermission(String permission) {
        permissions = Collections.singletonList(permission);
        return this;
    }

    @Override
    public CcorePermissionBuilder.MultiPermissionListener withPermissions(String... permissions) {
        this.permissions = Arrays.asList(permissions);
        return this;
    }

    @Override
    public CcorePermissionBuilder.MultiPermissionListener withPermissions(Collection<String> permissions) {
        this.permissions = new ArrayList<>(permissions);
        return this;
    }

    @Override
    public CcorePermissionBuilder withListener(PermissionListener listener) {
        this.listener = new MultiplePermissionsListenerToPermissionListenerAdapter(listener);
        return this;
    }

    @Override
    public CcorePermissionBuilder withListener(MultiplePermissionsListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public CcorePermissionBuilder onSameThread() {
        shouldExecuteOnSameThread = true;
        return this;
    }

    @Override
    public CcorePermissionBuilder withErrorListener(PermissionRequestErrorListener errorListener) {
        this.errorListener = errorListener;
        return this;
    }

    @Override
    public void check() {
        try {
            Thread thread = getThread();
            instance.checkPermissions(listener, permissions, thread);
        } catch (CcorePermissionException e) {
            errorListener.onError(e.error);
        }
    }

    private Thread getThread() {
        Thread thread;

        if (shouldExecuteOnSameThread) {
            thread = ThreadFactory.makeSameThread();
        } else {
            thread = ThreadFactory.makeMainThread();
        }

        return thread;
    }

    private static void initialize(Context context) {
        if (instance == null) {
            AndroidPermissionService androidPermissionService = new AndroidPermissionService();
            IntentProvider intentProvider = new IntentProvider();
            instance = new CcorePermissionInstance(context, androidPermissionService, intentProvider);
        } else {
            instance.setContext(context);
        }
    }

    static void onActivityReady(Activity activity) {
        if (instance != null) {
            instance.onActivityReady(activity);
        }
    }

    static void onActivityDestroyed(CcorePermissionActivity oldActivity) {
        if (instance != null) {
            instance.onActivityDestroyed(oldActivity);
        }
    }

    static void onPermissionsRequested(Collection<String> grantedPermissions,
                                       Collection<String> deniedPermissions) {
        if (instance != null) {
            instance.onPermissionRequestGranted(grantedPermissions);
            instance.onPermissionRequestDenied(deniedPermissions);
        }
    }
}
