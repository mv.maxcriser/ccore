package com.thecriser.ccore.permissions.listener.multi;

import com.thecriser.ccore.permissions.MultiplePermissionsReport;
import com.thecriser.ccore.permissions.PermissionToken;
import com.thecriser.ccore.permissions.listener.PermissionRequest;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CompositeMultiplePermissionsListener implements MultiplePermissionsListener {

    private final Collection<MultiplePermissionsListener> listeners;

    public CompositeMultiplePermissionsListener(MultiplePermissionsListener... listeners) {
        this(Arrays.asList(listeners));
    }

    public CompositeMultiplePermissionsListener(Collection<MultiplePermissionsListener> listeners) {
        this.listeners = listeners;
    }

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        for (MultiplePermissionsListener listener : listeners) {
            listener.onPermissionsChecked(report);
        }
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                   PermissionToken token) {
        for (MultiplePermissionsListener listener : listeners) {
            listener.onPermissionRationaleShouldBeShown(permissions, token);
        }
    }
}
