package com.thecriser.ccore.permissions;

import com.thecriser.ccore.permissions.listener.PermissionRequestErrorListener;
import com.thecriser.ccore.permissions.listener.multi.MultiplePermissionsListener;
import com.thecriser.ccore.permissions.listener.single.PermissionListener;

import java.util.Collection;

public interface CcorePermissionBuilder {

    CcorePermissionBuilder onSameThread();

    CcorePermissionBuilder withErrorListener(PermissionRequestErrorListener errorListener);

    void check();

    interface Permission {
        SinglePermissionListener withPermission(String permission);

        MultiPermissionListener withPermissions(String... permissions);

        MultiPermissionListener withPermissions(Collection<String> permissions);
    }

    interface SinglePermissionListener {
        CcorePermissionBuilder withListener(PermissionListener listener);
    }

    interface MultiPermissionListener {
        CcorePermissionBuilder withListener(MultiplePermissionsListener listener);
    }
}