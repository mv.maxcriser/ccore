package com.thecriser.ccore.permissions.listener;

import androidx.annotation.NonNull;

public final class PermissionRequest {

    private final String name;

    public PermissionRequest(@NonNull String name) {
        this.name = name;
    }

  public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Permission name: " + name;
    }
}
