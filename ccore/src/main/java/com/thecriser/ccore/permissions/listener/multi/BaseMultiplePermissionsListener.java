package com.thecriser.ccore.permissions.listener.multi;

import com.thecriser.ccore.permissions.MultiplePermissionsReport;
import com.thecriser.ccore.permissions.PermissionToken;
import com.thecriser.ccore.permissions.listener.PermissionRequest;

import java.util.List;

public class BaseMultiplePermissionsListener implements MultiplePermissionsListener {

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {

    }

    @Override
    public void onPermissionRationaleShouldBeShown(
            List<PermissionRequest> permissions,
            PermissionToken token
    ) {
        token.continuePermissionRequest();
    }
}
