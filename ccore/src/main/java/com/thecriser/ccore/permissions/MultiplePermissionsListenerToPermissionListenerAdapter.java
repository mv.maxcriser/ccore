package com.thecriser.ccore.permissions;

import com.thecriser.ccore.permissions.listener.PermissionDeniedResponse;
import com.thecriser.ccore.permissions.listener.PermissionGrantedResponse;
import com.thecriser.ccore.permissions.listener.PermissionRequest;
import com.thecriser.ccore.permissions.listener.multi.MultiplePermissionsListener;
import com.thecriser.ccore.permissions.listener.single.PermissionListener;

import java.util.List;

final class MultiplePermissionsListenerToPermissionListenerAdapter
        implements MultiplePermissionsListener {

    private final PermissionListener listener;

    MultiplePermissionsListenerToPermissionListenerAdapter(PermissionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        List<PermissionDeniedResponse> deniedResponses = report.getDeniedPermissionResponses();
        List<PermissionGrantedResponse> grantedResponses = report.getGrantedPermissionResponses();

        if (!deniedResponses.isEmpty()) {
            PermissionDeniedResponse response = deniedResponses.get(0);
            listener.onPermissionDenied(response);
        } else {
            PermissionGrantedResponse response = grantedResponses.get(0);
            listener.onPermissionGranted(response);
        }
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> requests,
                                                   PermissionToken token) {
        PermissionRequest firstRequest = requests.get(0);
        listener.onPermissionRationaleShouldBeShown(firstRequest, token);
    }
}
