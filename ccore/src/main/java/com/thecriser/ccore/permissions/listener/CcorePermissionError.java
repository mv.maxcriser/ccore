package com.thecriser.ccore.permissions.listener;

public enum CcorePermissionError {

    REQUEST_ONGOING,

    NO_PERMISSIONS_REQUESTED
}
