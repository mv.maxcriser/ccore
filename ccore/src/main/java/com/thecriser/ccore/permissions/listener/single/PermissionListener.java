package com.thecriser.ccore.permissions.listener.single;

import com.thecriser.ccore.permissions.PermissionToken;
import com.thecriser.ccore.permissions.listener.PermissionDeniedResponse;
import com.thecriser.ccore.permissions.listener.PermissionGrantedResponse;
import com.thecriser.ccore.permissions.listener.PermissionRequest;

public interface PermissionListener {

  void onPermissionGranted(PermissionGrantedResponse response);

  void onPermissionDenied(PermissionDeniedResponse response);

  void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token);

}
