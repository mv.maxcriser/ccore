package com.thecriser.ccore.permissions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.core.content.PermissionChecker;

import com.thecriser.ccore.permissions.listener.CcorePermissionError;
import com.thecriser.ccore.permissions.listener.PermissionDeniedResponse;
import com.thecriser.ccore.permissions.listener.PermissionGrantedResponse;
import com.thecriser.ccore.permissions.listener.PermissionRequest;
import com.thecriser.ccore.permissions.listener.multi.BaseMultiplePermissionsListener;
import com.thecriser.ccore.permissions.listener.multi.MultiplePermissionsListener;
import com.thecriser.ccore.permissions.listener.single.PermissionListener;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;

final class CcorePermissionInstance {

    private static final int PERMISSIONS_REQUEST_CODE = 42;
    private static final MultiplePermissionsListener EMPTY_LISTENER =
            new BaseMultiplePermissionsListener();

    private WeakReference<Context> context;
    private final AndroidPermissionService androidPermissionService;
    private final IntentProvider intentProvider;
    private final Collection<String> pendingPermissions;
    private final MultiplePermissionsReport multiplePermissionsReport;
    private final AtomicBoolean isRequestingPermission;
    private final AtomicBoolean rationaleAccepted;
    private final AtomicBoolean isShowingNativeDialog;
    private final Object pendingPermissionsMutex = new Object();

    private Activity activity;
    private MultiplePermissionsListener listener = EMPTY_LISTENER;

    CcorePermissionInstance(Context context, AndroidPermissionService androidPermissionService,
                            IntentProvider intentProvider) {
        this.androidPermissionService = androidPermissionService;
        this.intentProvider = intentProvider;
        this.pendingPermissions = new TreeSet<>();
        this.multiplePermissionsReport = new MultiplePermissionsReport();
        this.isRequestingPermission = new AtomicBoolean();
        this.rationaleAccepted = new AtomicBoolean();
        this.isShowingNativeDialog = new AtomicBoolean();
        setContext(context);
    }

    void setContext(Context context) {
        this.context = new WeakReference<>(context);
    }

    void checkPermission(PermissionListener listener, String permission, Thread thread) {
        checkSinglePermission(listener, permission, thread);
    }

    void checkPermissions(MultiplePermissionsListener listener, Collection<String> permissions,
                          Thread thread) {
        checkMultiplePermissions(listener, permissions, thread);
    }

    void onActivityReady(Activity activity) {
        this.activity = activity;

        PermissionStates permissionStates = null;
        synchronized (pendingPermissionsMutex) {
            if (activity != null) {
                permissionStates = getPermissionStates(pendingPermissions);
            }
        }

        if (permissionStates != null) {
            handleDeniedPermissions(permissionStates.getDeniedPermissions());
            updatePermissionsAsDenied(permissionStates.getImpossibleToGrantPermissions());
            updatePermissionsAsGranted(permissionStates.getGrantedPermissions());
        }
    }

    void onActivityDestroyed(Activity oldActivity) {
        if (activity == oldActivity) {
            activity = null;
            isRequestingPermission.set(false);
            rationaleAccepted.set(false);
            isShowingNativeDialog.set(false);
            listener = EMPTY_LISTENER;
        }
    }

    void onPermissionRequestGranted(Collection<String> permissions) {
        updatePermissionsAsGranted(permissions);
    }

    void onPermissionRequestDenied(Collection<String> permissions) {
        updatePermissionsAsDenied(permissions);
    }

    void onContinuePermissionRequest() {
        rationaleAccepted.set(true);
        requestPermissionsToSystem(pendingPermissions);
    }

    void onCancelPermissionRequest() {
        rationaleAccepted.set(false);
        updatePermissionsAsDenied(pendingPermissions);
    }

    private void requestPermissionsToSystem(Collection<String> permissions) {
        if (!isShowingNativeDialog.get()) {
            androidPermissionService.requestPermissions(activity,
                    permissions.toArray(new String[0]), PERMISSIONS_REQUEST_CODE);
        }
        isShowingNativeDialog.set(true);
    }

    private PermissionStates getPermissionStates(Collection<String> pendingPermissions) {
        PermissionStates permissionStates = new PermissionStates();

        for (String permission : pendingPermissions) {
            int permissionState = checkSelfPermission(activity, permission);

            switch (permissionState) {
                case PermissionChecker.PERMISSION_DENIED_APP_OP:
                    permissionStates.addImpossibleToGrantPermission(permission);
                    break;
                case PermissionChecker.PERMISSION_DENIED:
                    permissionStates.addDeniedPermission(permission);
                    break;
                case PermissionChecker.PERMISSION_GRANTED:
                default:
                    permissionStates.addGrantedPermission(permission);
                    break;
            }
        }

        return permissionStates;
    }

    private int checkSelfPermission(Activity activity, String permission) {
        try {
            return androidPermissionService.checkSelfPermission(activity, permission);
        } catch (RuntimeException ignored) {
            return PackageManager.PERMISSION_DENIED;
        }
    }

    private void startTransparentActivityIfNeeded() {
        Context context = this.context.get();
        if (context == null) {
            return;
        }

        Intent intent = intentProvider.get(context, CcorePermissionActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    private void handleDeniedPermissions(Collection<String> permissions) {
        if (permissions.isEmpty()) {
            return;
        }

        List<PermissionRequest> shouldShowRequestRationalePermissions = new LinkedList<>();

        for (String permission : permissions) {
            if (androidPermissionService.shouldShowRequestPermissionRationale(activity, permission)) {
                shouldShowRequestRationalePermissions.add(new PermissionRequest(permission));
            }
        }

        if (shouldShowRequestRationalePermissions.isEmpty()) {
            requestPermissionsToSystem(permissions);
        } else if (!rationaleAccepted.get()) {
            PermissionRationaleToken permissionToken = new PermissionRationaleToken(this);
            listener.onPermissionRationaleShouldBeShown(shouldShowRequestRationalePermissions,
                    permissionToken);
        }
    }

    private void updatePermissionsAsGranted(Collection<String> permissions) {
        for (String permission : permissions) {
            PermissionGrantedResponse response = PermissionGrantedResponse.from(permission);
            multiplePermissionsReport.addGrantedPermissionResponse(response);
        }
        onPermissionsChecked(permissions);
    }

    private void updatePermissionsAsDenied(Collection<String> permissions) {
        for (String permission : permissions) {
            PermissionDeniedResponse response = PermissionDeniedResponse.from(permission,
                    androidPermissionService.isPermissionPermanentlyDenied(activity, permission));
            multiplePermissionsReport.addDeniedPermissionResponse(response);
        }
        onPermissionsChecked(permissions);
    }

    private void onPermissionsChecked(Collection<String> permissions) {
        if (pendingPermissions.isEmpty()) {
            return;
        }

        synchronized (pendingPermissionsMutex) {
            pendingPermissions.removeAll(permissions);
            if (pendingPermissions.isEmpty()) {
              if (activity != null) {
                    activity.finish();
                }
                isRequestingPermission.set(false);
                rationaleAccepted.set(false);
                isShowingNativeDialog.set(false);
                MultiplePermissionsListener currentListener = listener;
                listener = EMPTY_LISTENER;
                currentListener.onPermissionsChecked(multiplePermissionsReport);
            }
        }
    }

    private void checkNoCcorePermissionRequestOngoing() {
        if (isRequestingPermission.getAndSet(true)) {
            throw new CcorePermissionException("Only one CcorePermission request at a time is allowed",
                    CcorePermissionError.REQUEST_ONGOING);
        }
    }

    private void checkRequestSomePermission(Collection<String> permissions) {
        if (permissions.isEmpty()) {
            throw new CcorePermissionException("CcorePermission has to be called with at least one permission",
                    CcorePermissionError.NO_PERMISSIONS_REQUESTED);
        }
    }

    private void checkSinglePermission(PermissionListener listener, String permission,
                                       Thread thread) {
        MultiplePermissionsListener adapter =
                new MultiplePermissionsListenerToPermissionListenerAdapter(listener);
        checkMultiplePermissions(adapter, Collections.singleton(permission), thread);
    }

    private void checkMultiplePermissions(final MultiplePermissionsListener listener,
                                          final Collection<String> permissions, Thread thread) {
        checkNoCcorePermissionRequestOngoing();
        checkRequestSomePermission(permissions);

        if (context.get() == null) {
            return;
        }

        if (activity != null && activity.isFinishing()) {
            onActivityDestroyed(activity);
        }

        pendingPermissions.clear();
        pendingPermissions.addAll(permissions);
        multiplePermissionsReport.clear();
        this.listener = new MultiplePermissionListenerThreadDecorator(listener, thread);
        if (isEveryPermissionGranted(permissions, context.get())) {
            thread.execute(new Runnable() {
                @Override
                public void run() {
                    MultiplePermissionsReport report = new MultiplePermissionsReport();
                    for (String permission : permissions) {
                        report.addGrantedPermissionResponse(PermissionGrantedResponse.from(permission));
                    }
                    isRequestingPermission.set(false);
                    listener.onPermissionsChecked(report);
                    CcorePermissionInstance.this.listener = EMPTY_LISTENER;
                }
            });
        } else {
            startTransparentActivityIfNeeded();
        }
        thread.loop();
    }

    private boolean isEveryPermissionGranted(Collection<String> permissions, Context context) {
        for (String permission : permissions) {
            int permissionState = androidPermissionService.checkSelfPermission(context, permission);
            if (permissionState != PermissionChecker.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private final class PermissionStates {
        private final Collection<String> deniedPermissions = new LinkedList<>();
        private final Collection<String> impossibleToGrantPermissions = new LinkedList<>();
        private final Collection<String> grantedPermissions = new LinkedList<>();

        private void addDeniedPermission(String permission) {
            deniedPermissions.add(permission);
        }

        private void addImpossibleToGrantPermission(String permission) {
            impossibleToGrantPermissions.add(permission);
        }

        private void addGrantedPermission(String permission) {
            grantedPermissions.add(permission);
        }

        private Collection<String> getDeniedPermissions() {
            return deniedPermissions;
        }

        private Collection<String> getGrantedPermissions() {
            return grantedPermissions;
        }

        public Collection<String> getImpossibleToGrantPermissions() {
            return impossibleToGrantPermissions;
        }
    }
}
