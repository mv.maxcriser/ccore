package com.thecriser.ccore.permissions;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;

class AndroidPermissionService {

    int checkSelfPermission(@NonNull Context context, @NonNull String permission) {
        return PermissionChecker.checkSelfPermission(context, permission);
    }

    void requestPermissions(@Nullable Activity activity, @NonNull String[] permissions,
                            int requestCode) {
        if (activity == null) {
            return;
        }

        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }

  boolean shouldShowRequestPermissionRationale(@Nullable Activity activity,
                                                 @NonNull String permission) {
        if (activity == null) {
            return false;
        }

        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }

    boolean isPermissionPermanentlyDenied(@Nullable Activity activity,
                                          @NonNull String permission) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }

        return !shouldShowRequestPermissionRationale(activity, permission);
    }
}
