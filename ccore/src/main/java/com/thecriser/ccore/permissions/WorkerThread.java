package com.thecriser.ccore.permissions;

import android.os.Handler;
import android.os.Looper;

final class WorkerThread implements Thread {

    private final Handler handler;
    private boolean wasLooperNull = false;

    WorkerThread() {
        if (Looper.myLooper() == null) {
            wasLooperNull = true;
            Looper.prepare();
        }
        handler = new Handler();
    }

    @Override
    public void execute(final Runnable runnable) {
        handler.post(runnable);
    }

    @Override
    public void loop() {
        if (wasLooperNull) {
            Looper.loop();
        }
    }
}
