package com.thecriser.ccore.permissions.listener.single;

import com.thecriser.ccore.permissions.PermissionToken;
import com.thecriser.ccore.permissions.listener.PermissionDeniedResponse;
import com.thecriser.ccore.permissions.listener.PermissionGrantedResponse;
import com.thecriser.ccore.permissions.listener.PermissionRequest;

import java.util.Arrays;
import java.util.Collection;

public class CompositePermissionListener implements PermissionListener {

    private final Collection<PermissionListener> listeners;

    public CompositePermissionListener(PermissionListener... listeners) {
        this(Arrays.asList(listeners));
    }

  public CompositePermissionListener(Collection<PermissionListener> listeners) {
        this.listeners = listeners;
    }

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {
        for (PermissionListener listener : listeners) {
            listener.onPermissionGranted(response);
        }
    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {
        for (PermissionListener listener : listeners) {
            listener.onPermissionDenied(response);
        }
    }

    @Override
    public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                   PermissionToken token) {
        for (PermissionListener listener : listeners) {
            listener.onPermissionRationaleShouldBeShown(permission, token);
        }
    }
}
