package com.thecriser.ccore.permissions;

import com.thecriser.ccore.permissions.listener.PermissionRequest;
import com.thecriser.ccore.permissions.listener.multi.MultiplePermissionsListener;

import java.util.List;

final class MultiplePermissionListenerThreadDecorator implements MultiplePermissionsListener {

    private final MultiplePermissionsListener listener;
    private final Thread thread;

    MultiplePermissionListenerThreadDecorator(MultiplePermissionsListener listener,
                                              Thread thread) {
        this.thread = thread;
        this.listener = listener;
    }

    @Override
    public void onPermissionsChecked(final MultiplePermissionsReport report) {
        thread.execute(new Runnable() {
            @Override
            public void run() {
                listener.onPermissionsChecked(report);
            }
        });
    }

  @Override
    public void onPermissionRationaleShouldBeShown(
            final List<PermissionRequest> permissions, final PermissionToken token) {
        thread.execute(new Runnable() {
            @Override
            public void run() {
                listener.onPermissionRationaleShouldBeShown(permissions, token);
            }
        });
    }
}
