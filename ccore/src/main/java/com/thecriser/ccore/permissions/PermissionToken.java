package com.thecriser.ccore.permissions;

public interface PermissionToken {

    void continuePermissionRequest();

    void cancelPermissionRequest();
}
