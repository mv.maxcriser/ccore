package com.thecriser.ccore.permissions.listener;

import androidx.annotation.NonNull;

public final class PermissionDeniedResponse {

    private final PermissionRequest requestedPermission;
    private final boolean permanentlyDenied;

    public PermissionDeniedResponse(@NonNull PermissionRequest requestedPermission,
                                    boolean permanentlyDenied) {
        this.requestedPermission = requestedPermission;
        this.permanentlyDenied = permanentlyDenied;
    }

    public static PermissionDeniedResponse from(@NonNull String permission,
                                                boolean permanentlyDenied) {
        return new PermissionDeniedResponse(new PermissionRequest(permission), permanentlyDenied);
    }

    public PermissionRequest getRequestedPermission() {
        return requestedPermission;
    }

    public String getPermissionName() {
        return requestedPermission.getName();
    }

    public boolean isPermanentlyDenied() {
        return permanentlyDenied;
    }
}
