package com.thecriser.ccore

import org.koin.core.module.Module

interface IAppCcore {

    val koinModules: List<Module>

    fun initAfterOnCreate()

    fun initBeforeOnCreate()

}