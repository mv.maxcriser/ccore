package com.thecriser.ccore.networking.upload

import android.os.Handler
import android.os.Looper
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream
import java.io.IOException

class UploadProgressRequestBody(
    private val file: File,
    private val contentType: String,
    private val callback: UploadCallback
) : RequestBody() {

    interface UploadCallback {
        fun onProgressUpdate(percentage: Int)
        fun onError()
        fun onFinish()
    }

    override fun contentType(): MediaType? {
        return "$contentType/*".toMediaTypeOrNull()
    }

    @Throws(IOException::class)
    override fun contentLength(): Long {
        return file.length()
    }

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        val fileLength = file.length()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val inputStream = FileInputStream(file)
        var uploaded: Long = 0

        inputStream.use { input ->
            var read: Int
            val handler = Handler(Looper.getMainLooper())

            while (input.read(buffer).also { read = it } != -1) {
                handler.post(ProgressUpdater(uploaded, fileLength))

                uploaded += read.toLong()

                sink.write(buffer, 0, read)
            }
        }
    }

    private inner class ProgressUpdater(private val uploaded: Long, private val total: Long) :
        Runnable {
        override fun run() {
            callback.onProgressUpdate((100 * uploaded / total).toInt())
        }

    }

    companion object {
        private const val DEFAULT_BUFFER_SIZE = 2048
    }
}
