package com.thecriser.ccore.networking

data class OkHttpHeader(
    val name: String,
    val value: String
)