package com.thecriser.ccore.networking.upload

import okhttp3.MultipartBody
import java.io.File

object UploadFileManager {

    @JvmStatic
    fun prepareFilePart(
        file: File,
        partName: String,
        contentType: String,
        callback: UploadProgressRequestBody.UploadCallback
    ): MultipartBody.Part {
        UploadProgressRequestBody(file, contentType, callback).apply {
            return MultipartBody.Part.createFormData(partName, file.name, this)
        }
    }

    @JvmStatic
    fun prepareFileParts(
        files: List<File>,
        partName: String,
        contentType: String,
        callback: UploadProgressRequestBody.UploadCallback
    ): List<MultipartBody.Part> {
        val parts: MutableList<MultipartBody.Part> = ArrayList()

        files.forEach {
            parts.add(prepareFilePart(it, partName, contentType, callback))
        }

        return parts
    }
}