package com.thecriser.ccore.networking

interface NetworkCallback<T> {

    fun onSuccessful(response: T?)

    fun onFailure(throwable: NetworkThrowable)

}
