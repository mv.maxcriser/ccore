package com.thecriser.ccore.networking

import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class AbstractNetworkExecutable<MODEL, NETWORK_API>(
    val callback: NetworkCallback<MODEL>?
) : KoinComponent {

    private val networkService: NetworkService<NETWORK_API> by inject()

    fun execute() {
        call().enqueue(object : Callback<MODEL?> {
            override fun onResponse(call: Call<MODEL?>, response: Response<MODEL?>) {
                if (response.isSuccessful) {
                    callback?.onSuccessful(response.body())
                } else {
                    callback?.onFailure(
                        NetworkThrowable(
                            response.code(),
                            response.errorBody()?.string(),
                            Throwable("Response is not successful [${response.code()}]")
                        )
                    )
                }
            }

            override fun onFailure(call: Call<MODEL?>, t: Throwable) {
                callback?.onFailure(NetworkThrowable(throwable = t))
                t.printStackTrace()
            }
        })
    }

    fun api() = networkService.api(baseUrl)

    abstract fun call(): Call<MODEL>

    abstract val baseUrl: String

}
