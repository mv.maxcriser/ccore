package com.thecriser.ccore.networking.handler

import retrofit2.Response

interface IResponseHandler {

    fun isSuccessful(response: Response<*>): Boolean

    fun processSuccessfulResponse(response: Response<*>): Any

    fun processFailureResponse()

    fun processFailure()

}