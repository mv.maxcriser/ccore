package com.thecriser.ccore.networking


data class NetworkThrowable(
    val responseCode: Int? = null,
    val errorBody: String? = null,
    val throwable: Throwable
) {
    fun errorMessage(): String? = if (errorBody.isNullOrEmpty()) throwable.localizedMessage else errorBody
}
