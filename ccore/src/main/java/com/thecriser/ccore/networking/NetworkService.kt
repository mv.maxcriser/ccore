package com.thecriser.ccore.networking

import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class NetworkService<NETWORK_API>(
    private val api: Class<NETWORK_API>,
    private val headersFunction: () -> List<OkHttpHeader>
) {

    private val loggingInterceptor = HttpLoggingInterceptor()

    private val client = OkHttpClient
            .Builder()
            .addInterceptor { chain ->
        val requestBuilder: Request.Builder = chain
            .request()
            .newBuilder()

        headersFunction.invoke().forEach {
            requestBuilder.addHeader(it.name, it.value)
        }

        chain.proceed(requestBuilder.build())
    }.addInterceptor(loggingInterceptor)

    private fun retrofit(url: String) = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .addConverterFactory(EnumConverterFactory())
        .client(client.build())

    fun api(url: String): NETWORK_API = retrofit(url)
        .baseUrl(url)
        .build()
        .create(api)

    init {
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    }
}
