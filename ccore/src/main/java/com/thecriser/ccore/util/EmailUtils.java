package com.thecriser.ccore.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class EmailUtils {

    public static void sendEmail(
            final Context context,
            final String to,
            final String subject,
            final String message
    ) {
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + subject + "&body=" + message + "&to=" + to);
        testIntent.setData(data);
        context.startActivity(testIntent);
    }
}
