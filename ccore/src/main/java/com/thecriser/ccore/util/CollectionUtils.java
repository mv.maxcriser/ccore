package com.thecriser.ccore.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class CollectionUtils {

    public static <T> void sort(List<T> from, Comparator<T> comparator) {
        if (from != null) {
            Collections.sort(from, comparator);
        }
    }

    public static boolean isEmpty(Collection list) {
        return list == null || list.isEmpty();
    }

    public static boolean isNotEmpty(Collection list) {
        return !isEmpty(list);
    }

    public static <T> void copy(Collection<T> from, List<T> to) {
        if (to == null) {
            to = new ArrayList<>();
        }

        if (from == null) {
            from = new ArrayList<>();
        }

        to.clear();
        to.addAll(from);
    }
}
