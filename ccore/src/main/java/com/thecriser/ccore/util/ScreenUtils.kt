package com.thecriser.ccore.util

import android.content.Context
import android.util.DisplayMetrics
import android.view.Display
import android.view.WindowManager
import kotlin.math.ceil

object ScreenUtils {

    fun getStatusBarHeight(context: Context): Int {
        return ceil(25 * context.resources.displayMetrics.density).toInt()
    }

    fun getScreenHeight(context: Context): Int {
        val wm: WindowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display: Display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)

        return metrics.heightPixels
    }

    fun getScreenWidth(context: Context): Int {
        val wm: WindowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display: Display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)

        return metrics.widthPixels
    }

}
