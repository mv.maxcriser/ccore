package com.thecriser.ccore.util

import android.os.Build

object VersionUtil {

    fun hasJellyBean(): Boolean = hasVersion(Build.VERSION_CODES.JELLY_BEAN) // 16

    fun hasKitKat(): Boolean = hasVersion(Build.VERSION_CODES.KITKAT) // 19

    fun hasLollipop(): Boolean = hasVersion(Build.VERSION_CODES.LOLLIPOP) // 21

    fun hasMarshmallow(): Boolean = hasVersion(Build.VERSION_CODES.M) // 23

    fun hasNougat(): Boolean = hasVersion(Build.VERSION_CODES.N) // 24

    fun hasOreo(): Boolean = hasVersion(Build.VERSION_CODES.O) // 26

    fun hasPie(): Boolean = hasVersion(Build.VERSION_CODES.P) // 28

    fun hasAndroid10(): Boolean = hasVersion(Build.VERSION_CODES.Q) // 29

    fun hasHoneycomb(): Boolean = hasVersion(Build.VERSION_CODES.HONEYCOMB) // 29

    private fun hasVersion(version: Int): Boolean = Build.VERSION.SDK_INT >= version

}