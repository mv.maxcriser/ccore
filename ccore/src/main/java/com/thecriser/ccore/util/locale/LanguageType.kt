package com.thecriser.ccore.util.locale

import com.thecriser.ccore.R

enum class LanguageType(
        val stringRes: Int,
        val code: String
) {

//    ARABIC(R.string.LANGUAGE_ARABIC, "ar"),
//    THAI(R.string.LANGUAGE_THAI, "th"),
//    KOREAN(R.string.LANGUAGE_KOREAN, "ko"),
    ENGLISH(R.string.LANGUAGE_ENGLISH, "en"), // done [spy fall/
    RUSSIAN(R.string.LANGUAGE_RUSSIAN, "ru"), // done [spy fall/
    TURKISH(R.string.LANGUAGE_TURKISH, "tr"), // done [spy fall/
    FRENCH(R.string.LANGUAGE_FRENCH, "fr"), // done [spy fall/
    PORTUGUESE(R.string.LANGUAGE_PORTUGUESE, "pt"), // done [spy fall/
    ITALIAN(R.string.LANGUAGE_ITALIAN, "it"),
    GERMAN(R.string.LANGUAGE_GERMAN, "de");

    companion object {

        @JvmStatic
        fun parse(value: String): LanguageType {
            val lg = values().find { language ->
                language.code == value
            }

            return lg ?: ENGLISH
        }
    }
}
