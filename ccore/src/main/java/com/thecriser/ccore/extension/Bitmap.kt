package com.thecriser.ccore.extension

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64
import java.io.*
import java.util.*

fun Bitmap.encodeToBase64(): String {
    val bitmap: Bitmap = this
    val byteArrayOutputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
    val b: ByteArray = byteArrayOutputStream.toByteArray()
    return Base64.encodeToString(b, Base64.DEFAULT)
}

fun String.decodeToBase64(): Bitmap? {
    val decodedByte: ByteArray = Base64.decode(this, 0)
    return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.size)
}

fun Bitmap.saveImageToExternalStorage(context: Context, name: String? = null): Uri? {
    val path = context.getExternalFilesDir(null).toString()
    val file = File(path, if (name.isNullOrEmpty()) "${UUID.randomUUID()}.jpg" else "$name.jpg")

    try {
        val stream: OutputStream = FileOutputStream(file)
        this.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        stream.flush()
        stream.close()
    } catch (e: IOException) { // Catch the exception
        e.printStackTrace()
    }

    return file.absolutePath.toSafeUri()
}
