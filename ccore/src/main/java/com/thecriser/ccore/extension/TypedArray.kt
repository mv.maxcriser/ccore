package com.thecriser.ccore.extension

import android.content.res.TypedArray

inline fun <T> TypedArray.withValueIfExist(
    index: Int,
    extractionMethod: TypedArray.(index: Int) -> T,
    action: (T) -> Unit
) {
    if (hasValue(index)) {
        action(extractionMethod(index))
    }
}

inline fun <T> TypedArray.withValueOrDefault(
    index: Int,
    extractionMethod: TypedArray.(index: Int) -> T,
    default: T,
    action: (T) -> Unit
) {
    if (hasValue(index)) {
        action(extractionMethod(index))
    } else {
        action(default)
    }
}

inline fun <T> TypedArray.withValueIfExist(
    index: Int,
    extractionMethod: TypedArray.(index: Int, default: T) -> T,
    default: T,
    action: (T) -> Unit
) {
    if (hasValue(index)) {
        action(extractionMethod(index, default))
    } else {
        action(default)
    }
}
