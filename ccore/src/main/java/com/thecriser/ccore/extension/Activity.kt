package com.thecriser.ccore.extension

import android.app.Activity

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
inline fun <reified S> Activity.getSystemService(name: String) = getSystemService(name) as S


inline fun <reified T> Activity.extra(key: String, default: T): Lazy<T?> = lazy(LazyThreadSafetyMode.NONE) {
    intent.extras?.get(key) as T ?: default
}

inline fun <reified T> Activity.extra(key: String): Lazy<T?> = lazy(LazyThreadSafetyMode.NONE) {
    intent.extras?.get(key) as? T
}

inline fun <reified T> Activity.requiredExtra(key: String): Lazy<T> = lazy(LazyThreadSafetyMode.NONE) {
    intent.extras?.get(key) as T ?: error("Required parameter for key $key was not passed!")
}
