package com.thecriser.ccore.extension

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_COMPACT
import androidx.core.text.HtmlCompat.fromHtml
import java.net.URLEncoder


fun String.parseHtml() = fromHtml(this, FROM_HTML_MODE_COMPACT)

fun String.openGoogleSearchQuery(context: Context?) {
    openIntentSearchQuery(context, "https://www.google.com/search?q=")
}

fun String.openIntentSearchQuery(context: Context?, url: String) {
    try {
        val escapedQuery: String = URLEncoder.encode(this, "UTF-8")
        val uri: Uri = Uri.parse("$url$escapedQuery")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context?.startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        Log.e("openIntentSearchQuery", e.localizedMessage ?: "Cannot open search intent, because of ActivityNotFoundException")
    }
}

fun String.openBrowserLink(context: Context?) {
    val uri: Uri = Uri.parse(this)
    val intent = Intent(Intent.ACTION_VIEW, uri)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    context?.startActivity(intent)
}

fun Char.isPrintableChar(): Boolean {
    val block = Character.UnicodeBlock.of(this)
    return (!Character.isISOControl(this)) && block != null && block != Character.UnicodeBlock.SPECIALS

}
