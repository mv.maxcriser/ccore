package com.thecriser.ccore.extension

import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.DrawableCompat

fun Drawable.tint(color: Int): Drawable {
    DrawableCompat.setTint(this, color)

    return this
}
