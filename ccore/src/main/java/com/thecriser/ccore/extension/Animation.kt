package com.thecriser.ccore.extension

import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation

fun View.scale(
    fromX: Float,
    toX: Float,
    fromY: Float,
    toY: Float,
    pivotXValue: Float = 0f,
    pivotYValue: Float = 1f,
    fillAfter: Boolean = true,
    duration: Long = 300
) {
    val anim: Animation = ScaleAnimation(
        fromX, toX,
        fromY, toY,
        Animation.RELATIVE_TO_SELF, pivotXValue,
        Animation.RELATIVE_TO_SELF, pivotYValue
    )

    anim.fillAfter = fillAfter
    anim.duration = duration
    startAnimation(anim)
}
