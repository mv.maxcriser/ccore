package com.thecriser.ccore.extension

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import com.thecriser.ccore.util.VersionUtil
import java.io.InputStream

fun Uri.toBitmap(context: Context): Bitmap = if (VersionUtil.hasPie()) {
    ImageDecoder.decodeBitmap(ImageDecoder.createSource(context.contentResolver, this))
} else {
    MediaStore.Images.Media.getBitmap(context.contentResolver, this)
}

fun Uri.toDrawable(context: Context): Drawable? = try {
    val inputStream: InputStream? = context.contentResolver.openInputStream(this)
    Drawable.createFromStream(inputStream, this.toString())
} catch (e: Exception) {
    null
}

fun String?.toSafeUri(): Uri? = if (this.isNullOrEmpty()) {
    null
} else {
    Uri.parse(this)
}
