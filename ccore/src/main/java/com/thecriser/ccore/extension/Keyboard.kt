package com.thecriser.ccore.extension

import android.app.Activity
import android.content.Context
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment


fun Activity.toggleKeyboard() {
    getSystemService<InputMethodManager>(Context.INPUT_METHOD_SERVICE)
        .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun Activity.hideKeyboard() {
    getSystemService<InputMethodManager>(Context.INPUT_METHOD_SERVICE).hideSoftInputFromWindow(
        currentFocus?.windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
}

fun Activity.showKeyboard(view: View) {
    if (view.requestFocus()) {
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun Context.hideKeyboard(view: View) {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.hideKeyboard(view: View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.toggleKeyboard() {
    activity?.toggleKeyboard()
}

fun Fragment.hideKeyboard() {
    activity?.hideKeyboard()
}

fun Fragment.hideKeyboard(view: View) {
    activity?.hideKeyboard(view)
}

fun Fragment.showKeyboard(view: View) {
    activity?.showKeyboard(view)
}

fun Context.isKeyboardShown(): Boolean {
    val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    return imm.isAcceptingText
}

fun Context.isMyInputMethodEnabled(): Boolean {
    return Settings.Secure.getString(contentResolver, Settings.Secure.DEFAULT_INPUT_METHOD).contains(packageName)
}

fun Context.isMyInputMethodTurnedOnInSettings(): Boolean {
    try {
        val imeManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        for (imi in imeManager.enabledInputMethodList) {
            if (imi.packageName == packageName) {
                return true
            }
        }

        return false
    } catch (e: Exception) {
        return false
    }
}
