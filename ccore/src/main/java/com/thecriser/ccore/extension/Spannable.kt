package com.thecriser.ccore.extension

import android.text.Spannable
import android.text.SpannableStringBuilder

inline fun buildSpannableString(builderAction: ModernSpannableStringBuilder.() -> Unit): ModernSpannableStringBuilder =
    ModernSpannableStringBuilder().apply(builderAction)

class ModernSpannableStringBuilder
@JvmOverloads constructor(text: CharSequence = "", start: Int = 0, end: Int = text.length) :
    SpannableStringBuilder(text, start, end), Appendable {

    fun append(text: CharSequence, what: Any): SpannableStringBuilder {
        val start = length
        append(text)
        setSpan(what, start, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    fun append(vararg values: CharSequence?): ModernSpannableStringBuilder {
        for (value in values) {
            append(value)
        }
        return this
    }
}
