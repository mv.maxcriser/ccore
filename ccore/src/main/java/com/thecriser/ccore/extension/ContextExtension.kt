package com.thecriser.ccore.extension

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat


private const val TEXT_PLAIN = "text/plain"
private const val IMAGE_PNG = "image/png"

const val PACKAGE_NAME_TWITTER = "com.twitter.android"
const val PACKAGE_NAME_FACEBOOK = "com.facebook.katana"
const val PACKAGE_NAME_INSTAGRAM = "com.instagram.android"

fun Context.toast(message: CharSequence?, duration: Int = Toast.LENGTH_SHORT) {
    message?.let {
        Toast.makeText(this, it, duration).show()
    }
}

fun Context.share(title: String?, shareBody: String?, subject: String?) {
    val sharingIntent = Intent(Intent.ACTION_SEND)
    sharingIntent.type = TEXT_PLAIN
    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
    startActivity(Intent.createChooser(sharingIntent, title))
}

fun Context.shareTwitter(message: String, errorFun: () -> Unit) {
    try {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = TEXT_PLAIN
        shareIntent.putExtra(Intent.EXTRA_TEXT, message) // set text

        shareIntent.setPackage(PACKAGE_NAME_TWITTER)
        startActivity(shareIntent)
    } catch (e: Exception) {
        errorFun.invoke()
    }
}

fun Context.shareFacebook(message: String, errorFun: () -> Unit) {
    try {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = TEXT_PLAIN
        shareIntent.putExtra(Intent.EXTRA_TEXT, message) // set text

        shareIntent.setPackage(PACKAGE_NAME_FACEBOOK)
        startActivity(shareIntent)
    } catch (e: Exception) {
        errorFun.invoke()
    }
}

fun Context.shareInstagram(pngUri: Uri?, errorFun: () -> Unit) {
    try {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = IMAGE_PNG
        shareIntent.putExtra(Intent.EXTRA_STREAM, pngUri) // set uri

        shareIntent.setPackage(PACKAGE_NAME_INSTAGRAM)
        startActivity(shareIntent)
    } catch (e: Exception) {
        errorFun.invoke()
    }
}

fun Context.openPlayMarket(packageName: String) {
    try {
        ContextCompat.startActivity(
            this,
            Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")),
            null
        )
    } catch (exception: ActivityNotFoundException) {
        ContextCompat.startActivity(
            this,
            Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")),
            null
        )
    }
}
