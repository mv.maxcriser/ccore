package com.thecriser.ccore.extension

import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thecriser.ccore.ui.recyclerview.SpaceItemDecoration

inline fun RecyclerView.spacing(configuration: SpaceItemDecoration.Configuration.() -> Unit) = SpaceItemDecoration(
    SpaceItemDecoration.Configuration(context.resources)
        .apply(configuration)
).also { addItemDecoration(it) }

fun RecyclerView.setDivider(@DrawableRes drawableResId: Int) = DividerItemDecoration(
    context,
    (layoutManager as? LinearLayoutManager)?.orientation ?: RecyclerView.VERTICAL
).apply {
    ContextCompat.getDrawable(context, drawableResId)?.let {
        setDrawable(it)
    }
}.also { addItemDecoration(it) }
