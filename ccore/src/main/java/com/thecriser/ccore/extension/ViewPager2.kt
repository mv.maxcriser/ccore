package com.thecriser.ccore.extension

import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

val ViewPager2.recyclerView
    get() = get(0) as RecyclerView
