package com.thecriser.ccore.extension

import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.EditText

fun EditText.processDelete() {
    dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
    dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
}

fun EditText.onDoneClick(funExecute: () -> Unit) {
    this.imeOptions = EditorInfo.IME_ACTION_DONE

    this.setOnEditorActionListener { _, actionId, _ ->
        when (actionId) {
            EditorInfo.IME_ACTION_DONE -> {
                funExecute.invoke()
                true
            }
            else -> false
        }
    }
}
