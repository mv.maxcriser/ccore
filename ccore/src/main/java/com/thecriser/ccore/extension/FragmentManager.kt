@file:JvmName("FragmentManagerExtension")

package com.thecriser.ccore.extension

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

@JvmOverloads
fun FragmentManager.replaceFragment(
    @IdRes containerViewId: Int,
    fragment: Fragment,
    backStack: Boolean? = false,
    tag: String
) {
    processFragment(containerViewId, fragment, backStack, tag, true)
}

@JvmOverloads
fun FragmentManager.processFragment(
    @IdRes containerViewId: Int,
    fragment: Fragment,
    backStack: Boolean? = false,
    tag: String? = null,
    isReplace: Boolean
) {
    val fragmentById = findFragmentById(containerViewId)

    if (
        fragmentById?.javaClass?.name == fragment.javaClass.name
        &&
        fragmentById?.tag == tag
    ) {
        return
    }

    val fragmentTransaction = beginTransaction()
    val findFragment = findFragmentByTag(tag)

    with(fragmentTransaction) {
        if (isReplace) {
            replace(containerViewId, fragment, tag)
        } else {
            add(containerViewId, fragment, tag)
        }

        if (findFragment == null) {
            if (backStack == true) {
                addToBackStack(tag)
            }
        }

        commitAllowingStateLoss()
    }
}

@JvmOverloads
fun FragmentManager.addFragment(
    @IdRes containerViewId: Int,
    fragment: Fragment,
    backStack: Boolean? = false,
    tag: String? = null
) {
    processFragment(containerViewId, fragment, backStack, tag, false)
}
