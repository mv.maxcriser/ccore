package com.thecriser.ccore.extension

import androidx.fragment.app.Fragment

inline fun <reified T> Fragment.extra(key: String, default: T): Lazy<T> = lazy(LazyThreadSafetyMode.NONE) {
    arguments?.get(key) as T ?: default
}

inline fun <reified T> Fragment.extra(key: String): Lazy<T?> = lazy(LazyThreadSafetyMode.NONE) {
    arguments?.get(key) as? T
}

inline fun <reified T> Fragment.requiredExtra(key: String): Lazy<T> = lazy(LazyThreadSafetyMode.NONE) {
    arguments?.get(key) as T ?: error("Required parameter for key $key was not passed!")
}

