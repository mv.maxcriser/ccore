package com.thecriser.ccore.extension

import android.graphics.Outline
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.FrameLayout
import androidx.annotation.DimenRes
import androidx.coordinatorlayout.widget.CoordinatorLayout


fun View.findSuitableSnackBarParent(): ViewGroup? {
    var view: View? = this
    var fallbackView: ViewGroup? = null

    do {
        when (view) {
            is CoordinatorLayout -> {
                // Мы нашли CoordinatorLayout, используем его
                return view
            }
            is FrameLayout -> {
                if (view.id == android.R.id.content) {
                    // Если мы дошли до Decor вью, значит мы не нашли CoordinatorLayout в иерархии, тогда используем Decor
                    return view
                } else {
                    // Это всё еще не Decor, сохраним его как резерв
                    fallbackView = view
                }
            }
            else -> {
                // Пытаемся иди вверх по иерархии
                view = view?.parent as? View
            }
        }
    } while (view != null)

    // Если мы здесь, значит не нашли подходящего ConstraintLayout или не дошли до Decor вью, используем резерв
    return fallbackView
}



fun View.setRoundedCorners(
    cornerRadius: Float,
    leftTop: Boolean = true,
    rightTop: Boolean = true,
    leftBottom: Boolean = true,
    rightBottom: Boolean = true
) {
    outlineProvider = object : ViewOutlineProvider() {

        override fun getOutline(view: View?, outline: Outline?) {

            val left = if (!leftBottom && !leftTop) -cornerRadius.toInt() else 0
            val top = if (!leftTop && !rightTop) -cornerRadius.toInt() else 0
            val right = if (!rightTop && !rightBottom) ((view?.width ?: 0) + cornerRadius).toInt() else (view?.width ?: 0).toInt()
            val bottom = if (!rightBottom && !leftBottom) ((view?.height ?: 0) + cornerRadius).toInt() else (view?.height ?: 0).toInt()

            outline?.setRoundRect(
                left,
                top,
                right,
                bottom,
                cornerRadius
            )
        }
    }
    clipToOutline = true
}

fun View.setRoundedCorners(
    @DimenRes cornerRadiusRes: Int,
    leftTop: Boolean = true,
    rightTop: Boolean = true,
    leftBottom: Boolean = true,
    rightBottom: Boolean = true
) = setRoundedCorners(
    context.resources.getDimension(cornerRadiusRes),
    leftTop,
    rightTop,
    leftBottom,
    rightBottom
)