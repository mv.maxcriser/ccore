package com.thecriser.ccore.extension

import android.app.Activity
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Activity.toast(message: String, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(this, message, duration).show()

fun Activity.toast(@StringRes messageResId: Int, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, messageResId, duration).show()


fun Fragment.toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    context?.let {
        Toast.makeText(it, message, duration).show()
    }
}

fun Fragment.toast(@StringRes messageResId: Int, duration: Int = Toast.LENGTH_SHORT) {
    context?.let {
        Toast.makeText(it, messageResId, duration).show()
    }
}
