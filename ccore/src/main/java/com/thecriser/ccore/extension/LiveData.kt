package com.thecriser.ccore.extension

import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.debounce(duration: Long = 1000L) = MediatorLiveData<T>().also { mediatorLiveData ->
    val source = this
    val handler = Handler(Looper.getMainLooper())

    val runnable = Runnable {
        mediatorLiveData.value = source.value
    }

    mediatorLiveData.addSource(source) {
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, duration)
    }
}

inline fun <T> Fragment.observe(
    liveData: LiveData<T>,
    lifecycleOwner: LifecycleOwner = viewLifecycleOwner,
    crossinline observer: (T) -> Unit
) = liveData.observe(lifecycleOwner, Observer { observer(it) })

inline fun <T> AppCompatActivity.observe(
    liveData: LiveData<T>,
    crossinline observer: (T) -> Unit
) = liveData.observe(this, Observer { observer(it) })
