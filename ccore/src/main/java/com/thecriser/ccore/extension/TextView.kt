package com.thecriser.ccore.extension

import android.content.Context
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.annotation.FontRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible

fun TextView.setDrawables(
    left: Drawable? = null,
    top: Drawable? = null,
    right: Drawable? = null,
    bottom: Drawable? = null
) = setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom)

fun TextView.setTypeFace(context: Context, @FontRes fontRes: Int) {
    typeface = ResourcesCompat.getFont(context, fontRes)
}

fun TextView.underline() {
    paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
}

fun TextView.setTextOrHide(value: String?) {
    text = value
    isVisible = value.isNullOrEmpty().not()
}

fun TextView.setTextOrHide(value: Int?) {
    isVisible = if (value != null) {
        setText(value)
        true
    } else {
        false
    }
}
